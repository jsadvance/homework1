// ## Теоретичне питання
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Це коли обин обект наслідує властивості і методи іншого обекту
//

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// для отримання і змоху працювати з властивостями і методами які були створенні в батька

// ## Завдання
// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// 2. Створіть гетери та сеттери для цих властивостей.
// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

// ## Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Література:
// - [Класи на MDN](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Classes)
// - [Класи в ECMAScript 6](https://frontender.info/es6-classes-final/)

class Employee {
  #name
  #age
  #salary
  constructor(name, age, salary) {
    this.#name = name
    this.#age = age
    this.#salary = salary
  }

  set name(newName) {
    this.#name = newName
  }

  get name() {
    return this.#name
  }
  set age(newAge) {
    this.#age = newAge
  }

  get age() {
    return this.#age
  }
  set salary(newSalary) {
    this.#salary = newSalary
  }

  get salary() {
    return this.#salary
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, leng) {
    super(name, age, salary)
    this.leng = leng
  }

  get salary() {
    return super.salary * 3
  }
}

let employee1 = new Programmer('Sergey', 20, 2000, ['ua', 'en'])
let employee2 = new Programmer('Ira', 18, 1200, ['ua', 'en'])
let employee3 = new Programmer('Petro', 25, 3400, ['ua', 'en'])
let employee4 = new Programmer('Nadya', 30, 2500, ['ua', 'en'])
let employee5 = new Programmer('Maria', 32, 3500, ['ua', 'en'])

console.log(employee1)
console.log(employee2)
console.log(employee3)
console.log(employee4)
console.log(employee5)

console.log(employee1.salary)
console.log(employee2.salary)
console.log(employee3.salary)
console.log(employee4.salary)
console.log(employee5.salary)
